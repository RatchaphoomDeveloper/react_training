import React from "react";
import { Routes, Route } from "react-router-dom";
import { ROUTER_AUTH_MODEL } from "../models/Router_Model";
const UnauthsRouter = () => {
  return (
    <>
      <Routes>
        {ROUTER_AUTH_MODEL.filter((x) => x.is_active === true).map((x, i) => (
          <Route key={i} path={x.router_path} element={x.router_comp} />
        ))}
      </Routes>
    </>
  );
};

export default UnauthsRouter;
