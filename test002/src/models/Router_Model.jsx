import Loginpage from "../pages/auths/Loginpage";
import Registerpage from "../pages/auths/Registerpage";

export const ROUTER_AUTH_MODEL = [
  {
    id: 1,
    router_name: "Login",
    router_path: "/login",
    router_comp: <Loginpage />,
    router_icon: "",
    is_active: true,
  },
  {
    id: 2,
    router_name: "Register",
    router_path: "/register",
    router_comp: <Registerpage />,
    router_icon: "",
    is_active: true,
  },
];

export const ROUTER_USERS_MODEL = [];
