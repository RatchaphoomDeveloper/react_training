
export const USER_TYPE = [
    {
        id : 1,
        name : "Admin",
        name_th : "แอดมิน",
        is_active : true
    },
    {
        id: 2,
        name : "Customer",
        name_th : "ลูกค้า",
        is_active : true
    },
]

export const GENDERS = [
    {
        id : 1,
        name : "Male",
        name_th : "ชาย",
        is_active : true
    },
    {
        id : 2,
        name : "Female",
        name_th : "หญิง",
        is_active : true
    },
    {
        id : 3,
        name : "LGBT Q+",
        name_th : "หลากหลายทางเพศ",
        is_active : true
    }
]