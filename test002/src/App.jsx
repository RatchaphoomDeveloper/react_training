import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import UnauthsRouter from "./route/UnauthsRouter";

function App() {
  const [count, setCount] = useState(0);

  return (
    <div className="">
       <UnauthsRouter/>
    </div>
  );
}

export default App;
