import axios from "axios";
import { APP_CONFIG } from "../../env";

axios.defaults.baseURL = APP_CONFIG.WEBSERVICE;

axios.interceptors.request.use(
  (request) => {
    const token = localStorage.getItem("token");
    if (token) {
      request.headers.Authorization = token;
    }
    return request;
  },
  (error) => {
    if (error.response.status === 401) {
      window.location.href = "/login";
    }
    throw error;
  }
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      window.location.href = "/login";
    }
    throw error;
  }
);
