import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./index.css";
import { BrowserRouter } from "react-router-dom";

// import all config
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import config from "./store/store";
import "./configs/index";

const { store, persistor } = config();

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    {/* ถ้าไม่ import ตัวนี้จะไม่สามารถใข้งาน page ต่างๆได้  */}
    <BrowserRouter>
     {/* import provider */}
      <Provider store={store}>
        {/* import persisgate */}
        <PersistGate loading={null} persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);
