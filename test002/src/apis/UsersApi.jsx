import axios from "axios";
import { Component } from "react";


class UsersApi extends Component {
  static SelUserType = async (data) => {
    try {
      const result = await axios({
        url: "/select_user_types",
        method: "POST",
        data: data,
      });

      return result.data;
    } catch (error) {}
  };

  static Register = async (data) => {
    try {
      const result = await axios({
        url: "/register_user",
        method: "POST",
        data: data,
      });

      return result.data;
    } catch (error) {}
  };

  static Login = async (data) => {
    try {
      const result = await axios({
        url: "/login",
        method: "POST",
        data: data,
      });

      return result.data;
    } catch (error) {}
  };
}

export default UsersApi
