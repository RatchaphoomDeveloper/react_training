import React, { useRef } from "react";
import { useForm, Controller } from "react-hook-form";
import { Link } from "react-router-dom";
import { REGISTER_FORM_TYPE, REGISTER_FORM } from "../../models/Form_Model";

const Registerpage = () => {
  const {
    register,
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();

  // submit function
  const onSubmit = async (data) => {
    console.log(data);
  };

  return (
    <div className="w-full h-screen flex justify-center items-center bg-gradient-to-br from-cyan-500 to-blue-500">
      <form onSubmit={handleSubmit(onSubmit)}>
        {REGISTER_FORM_TYPE.filter((x) => x.is_active === true).map((x, i) => {
          return (
            <>
              <div
                key={x.id}
                className="border mt-[1rem] lg:w-[600px] md:w-[560px] w-[350px] h-auto rounded-lg shadow-lg p-[1rem]  md:mt-[1rem] bg-white"
              >
                <h1 className="  lg:text-[28px] md:text-[22px] text-[18px] text-left italic font-bold mb-[1.5rem]">
                  {x.name}
                </h1>
                {REGISTER_FORM.filter(
                  (xx) => xx.is_active && xx.register_form_type === x.id
                ).map((xx, xi) => {
                  return (
                    <>
                      <div key={xx.id}>
                        <label className="font-bold md:text-[14px] lg:text-[16px]  sm:text-md">{xx.name}</label>
                        <div className="mt-[.5rem]">
                          <input
                            className={`border w-full rounded-lg h-[35px] focus:outline-none pl-[1rem] ${
                              errors[xx.tag_name]
                                ? " border-red-500 border-2 duration-300 "
                                : ""
                            }`}
                            type={x.input_type}
                    
                            {...register(xx.tag_name,{required:true,validate:(val)=>{
                              if(watch('password') != val){
                                return "Your passwords do no match";
                              }
                            }})}
                          />
                        </div>
                      </div>
                      {errors[xx.tag_name] && (
                        <label className=" text-red-700 text-sm ">
                          {xx.error_message_en}
                        </label>
                      )}
                    </>
                  );
                })}
              </div>
            </>
          );
        })}
        <div className="mt-[.5rem]">
          <button
            type="submit"
            className="
                 shadow-lg
            border 
            w-full 
            h-[45px] 
            rounded-lg 
            bg-purple-500 
            text-white 
            font-bold text-lg
             cursor-pointer
             hover:bg-purple-800
             active:bg-purple-800
            "
          >
            Register
          </button>
        </div>
      </form>
    </div>
  );
};

export default Registerpage;
