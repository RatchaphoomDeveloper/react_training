import React, { useState, useEffect } from "react";
import { LOGIN_FORM_MODEL } from "../../models/Form_Model";
import { useForm, Controller } from "react-hook-form";
import { Link } from "react-router-dom";
import UsersApi from "../../apis/UsersApi";
import { Combobox, Listbox } from "@headlessui/react";
const Loginpage = () => {
  const [usertypes, setUserTypes] = useState([]);
  const [usertype, setUserType] = useState([]);
  const [query, setQuery] = useState("");
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const getUserTypes = async () => {
    let attibutes = ["id", "name_en", "is_active"];
    await UsersApi.SelUserType(attibutes).then((x) => {
      if (x.code === 1) {
        setUserTypes(x.data);
        setUserType(x.data[0]);
      }
    });
  };

  // submit function
  const onSubmit = async (data) => {
    let d = { ...data, user_type_id: usertype.id }
    await UsersApi.Login(d).then((x)=>{
      if(x.code === 1){
        alert('login is success')
      }
    })
  };

  useEffect(() => {
    getUserTypes();
  }, []);

  return (
    <div className="w-full h-screen flex justify-center items-center">
      <div className="border w-[500px] h-auto rounded-lg shadow-lg p-[1rem] sm:m-[1rem] md:m-[0rem]">
        <h1 className="text-[28px] text-center italic font-bold mb-[1.5rem]">
          BACKOFFICE CENTER
        </h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-col gap-3  ">
            {LOGIN_FORM_MODEL.filter((x) => x.is_active === true).map(
              (x, i) => {
                return (
                  <Controller
                    key={i}
                    name={x.tag_name}
                    control={control}
                    rules={{
                      required: x.is_require,
                    }}
                    render={({ field: { onChange, onBlur, value } }) => {
                      return (
                        <>
                          {x.input_type === "dropdown" ? (
                            <>
                              <div>
                                <label className="font-bold">{x.name}</label>
                                <div className=" border ">
                                  <Listbox
                                    value={usertype}
                                    onChange={setUserType}
                                  >
                                    <Listbox.Button>
                                      {usertype.name_en}
                                    </Listbox.Button>
                                    <Listbox.Options>
                                      {usertypes.map((res, ii) => {
                                        return (
                                          <Listbox.Option key={ii} value={res}>
                                            {res.name_en}
                                          </Listbox.Option>
                                        );
                                      })}
                                    </Listbox.Options>
                                  </Listbox>
                                </div>
                              </div>
                            </>
                          ) : (
                            <>
                              <div>
                                <label className="font-bold">{x.name}</label>
                                <div className="mt-[.5rem]">
                                  <input
                                    className={`border w-full rounded-lg h-[35px] focus:outline-none pl-[1rem] ${
                                      errors[x.tag_name]
                                        ? " border-red-500 border-2 duration-300 "
                                        : ""
                                    }`}
                                    type={x.input_type}
                                    value={value}
                                    onChange={onChange}
                                  />
                                </div>
                              </div>
                            </>
                          )}

                          {errors[x.tag_name] && (
                            <label className=" text-red-700 text-sm ">
                              {x.error_message_en}
                            </label>
                          )}
                        </>
                      );
                    }}
                  />
                );
              }
            )}
            <div className="">
              <label
                htmlFor=""
                className="text-sm text-gray-500 cursor-pointer hover:underline"
              >
                Forget password?
              </label>
            </div>
            <div className="mt-[.5rem]">
              <button
                type="submit"
                className="
                 shadow-lg
            border 
            w-full 
            h-[45px] 
            rounded-lg 
            bg-purple-500 
            text-white 
            font-bold text-lg
             cursor-pointer
             hover:bg-purple-800
             active:bg-purple-800
            "
              >
                Login
              </button>
            </div>
            <div className=" text-center ">
              <Link
                to="/register"
                className="text-sm text-gray-500 cursor-pointer hover:underline"
              >
                Register
              </Link>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Loginpage;
