const UserType = require("../model/UserTypeModel");

const InsUserType = async ({
  name_th,
  name_en,
  create_by,
  update_by,
  is_active,
}) => {
  try {
    let result;
    const usertypes = await UserType.create({
      name_th: name_th,
      name_en: name_en,
      create_by: create_by,
      update_by: update_by,
      is_active: is_active,
    });
    return {
      code: 1,
      message: "success",
      data: usertypes,
    };
  } catch (error) {
    return {
      code: -1,
      message: `InsUserType ${error}`,
      data: null,
    };
  }
};

const UpdUserType = async ({
  id,
  name_th,
  name_en,
  create_by,
  update_by,
  is_active,
}) => {
  try {
    const usertypes = await UserType.update(
      {
        name_th: name_th,
        name_en: name_en,
        create_by: create_by,
        update_by: update_by,
        is_active: is_active,
      },
      {
        where: { id: id },
      }
    );
    return {
      code: 1,
      message: "success",
      data: usertypes,
    };
  } catch (error) {
    return {
      code: -1,
      message: `InsUserType ${error}`,
      data: null,
    };
  }
};

const DelUserType = async ({ id }) => {
  try {
    const usertypes = await UserType.destroy({ where: { id: id } });
    return {
      code: 1,
      message: "success",
      data: usertypes,
    };
  } catch (error) {
    return {
      code: -1,
      message: `InsUserType ${error}`,
      data: null,
    };
  }
};

const SelUserType = async ({ autibutes }) => {
  try {
    const usertypes = await UserType.findAll({
      order: [["update_date", "DESC"]],
      attributes: autibutes,
    });
    return {
      code: 1,
      message: "success",
      data: usertypes,
    };
  } catch (error) {
    return {
      code: -1,
      message: `InsUserType ${error}`,
      data: null,
    };
  }
};

const SelByIdUserType = async ({ id, autibutes }) => {
  try {
    const usertypes = await UserType.findAll({
      where: { id: id },
      order: [["update_date", "DESC"]],
      attributes: autibutes,
    });
    return {
      code: 1,
      message: "success",
      data: usertypes,
    };
  } catch (error) {
    return {
      code: -1,
      message: `InsUserType ${error}`,
      data: null,
    };
  }
};

module.exports = {
  InsUserType,
  UpdUserType,
  DelUserType,
  SelUserType,
  SelByIdUserType,
};
