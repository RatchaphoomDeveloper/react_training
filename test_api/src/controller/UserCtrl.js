const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const APP_CONFIG = require("../../env");
const UserModel = require("../model/UserModel");

const RegisterUser = async ({
  user_type_id,
  username,
  password,
  name,
  lastname,
  phonenumber,
  address,
}) => {
  try {
    // var
    var encPassword = await bcrypt.hash(password, 10);
    const users = await UserModel.create({
      user_type_id,
      username,
      password: encPassword,
      name,
      lastname,
      phonenumber,
      address,
    });
    return {
      code: 1,
      message: "success",
      data: null,
    };
  } catch (error) {
    return {
      code: -1,
      message: `RegisterUser : ${error}`,
      data: null,
    };
  }
};

const LoginUser = async ({ user_type_id, username, password }) => {
  try {
    const users = await UserModel.findAll({
      where: { user_type_id: user_type_id, username: username },
    });
    if (users && (await bcrypt.compare(password, users[0].password))) {
      const token = await jwt.sign({ user_id: users.id }, APP_CONFIG.TOKEN, {
        expiresIn: "8h",
      });
      return {
        code: 1,
        message: "success",
        data:{ ...users[0].dataValues, token: token},
      };
    } else {
      return {
        code: -1,
        message: "user is not found",
        data: null,
      };
    }
  } catch (error) {
    return {
      code: -1,
      message: `LoginUser : ${error}`,
      data: null,
    };
  }
};

module.exports = {
  RegisterUser,
  LoginUser,
};
