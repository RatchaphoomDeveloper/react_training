const { DataTypes } = require("sequelize");
const sequelize = require("../db");
const UserType = require("./UserTypeModel");

const UserModel = sequelize.define(
  "M_USERS",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      field: "id",
    },
    user_type_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: "user_type_id",
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      unique: true,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      validate: {
        max: 20,
      },
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      validate: {
        max: 20,
      },
    },
    phonenumber: {
      type: DataTypes.STRING(10),
      allowNull: false,
      defaultValue: "",
    },
    address: {
      type: DataTypes.TEXT,
      allowNull: false,
      defaultValue: "",
    },
    create_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
      field: "create_date",
    },
    update_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
      field: "update_date",
    },
    create_by: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: "create_by",
    },
    update_by: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: "update_by",
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true,
      field: "is_active",
    },
  },
  {
    tableName: "M_USERS",
  }
);
// many to many
UserModel.belongsTo(UserType, {
  foreignKey: {
    name: "user_type_id",
    field: "user_type_id",
  },
});

module.exports = UserModel;
