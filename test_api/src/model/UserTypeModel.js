const { DataTypes } = require("sequelize");
const sequelize = require("../db");

// m_USER_TYPe

const UserType = sequelize.define(
  "M_USER_TYPE",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      unique: true,
      field: "id",
    },
    name_th: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      field: "name_th",
      unique : true
    },
    name_en: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      field: "name_en",
      unique : true
    },
    create_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
      field: "create_date",
    },
    update_date: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: new Date(),
      field: "update_date",
    },
    create_by: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: "create_by",
    },
    update_by: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: "update_by",
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true,
      field: "is_active",
    },
  },
  {
    tableName: "M_USER_TYPE",
  }
);

module.exports = UserType