const { Sequelize } = require("sequelize");
const APP_CONFIG = require("../../env");

const sequelize = new Sequelize({
  host: APP_CONFIG.DB.HOST,
  port: APP_CONFIG.DB.PORT,
  username: APP_CONFIG.DB.USERNAME,
  password: APP_CONFIG.DB.PASSWORD,
  database: APP_CONFIG.DB.DATABASE,
  dialect: "postgres",
  logging : true
});

module.exports = sequelize;
