var express = require("express");
const { InsUserType, UpdUserType, DelUserType, SelUserType, SelByIdUserType } = require("../src/controller/UserTypeCtrl");
var router = express.Router();

router.post("/save_user_type", async (req, res, next) => {
  let data = req.body;
  const resP = await InsUserType(data);
  res.status(200).json(resP);
});

router.post("/update_user_type", async (req, res, next) => {
  let data = req.body;
  const resP = await UpdUserType(data);
  res.status(200).json(resP);
});

router.post("/delete_user_type",async(req,res,next)=>{
  let data = req.body
  const resP = await DelUserType(data)
  res.status(200).json(resP);
})

router.post("/select_user_types",async(req,res,next)=>{
  let data = req.body
  const resP = await SelUserType(data)
  res.status(200).json(resP);
})

router.post("/select_id_user_types",async(req,res,next)=>{
  let data = req.body
  const resP = await SelByIdUserType(data)
  res.status(200).json(resP);
})

module.exports = router;
