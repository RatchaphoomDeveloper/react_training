var express = require("express");
const { RegisterUser, LoginUser } = require("../src/controller/UserCtrl");
var router = express.Router();

router.post("/register_user", async (req, res, next) => {
  let data = req.body;
  const resP = await RegisterUser(data);
  res.status(200).json(resP);
});
router.post("/update_user", async (req, res, next) => {});
router.post("/delete_user", async (req, res, next) => {});
router.post("/select_user", async (req, res, next) => {});
router.post("/select_id_user", async (req, res, next) => {});
router.post("/login", async (req, res, next) => {
  let data = req.body;
  const resP = await LoginUser(data);
  res.status(200).json(resP);
});

module.exports = router;
