var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
require("dotenv").config();
var sequelize = require("./src/db");

async function asyncDatabase(){
  // create all table
  await sequelize.sync({force : false});
  // restructure table
  // await UserModel.sync({force : true})
}
asyncDatabase();


var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var usertypesRouter = require("./routes/userstype");
const UserType = require("./src/model/UserTypeModel");
const UserModel = require("./src/model/UserModel");
var app = express();

app.use(logger("dev"));
app.use(
  cors({
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE", "PATCH"],
    credentials: true,
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/api", usersRouter);
app.use("/api", usertypesRouter);

module.exports = app;
